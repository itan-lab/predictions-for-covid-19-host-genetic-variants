COVID-19 predictions

Predictions indicating variants resulting in COVID-19, neutral or non-COVID-19 pathogenic variants for 71,661,823 missense mutations in the human genome. Variant coordinates correspond to the GRCh38/hg38 genome assembly.

Columns 'COVID19', 'Neutral', 'Non-COVID19 Pathogenic' correspond to the predicted probability that a variant is COVID-19-associated variant, 'Neutral', or non-COVID-19 pathogenic variant respectively. We recommend a cutoff of .5 for assigning labels to variants - higher scores indicating higher probability as predicted by the model.